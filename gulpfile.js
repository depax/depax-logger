/**
 * Provides the generic depax gulp tasks.
 */

"use strict";

const gulp = require("gulp");
const pkg = require("./package.json");

require("@depax/dev-tools")(pkg, gulp);
