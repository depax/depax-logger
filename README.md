# Depax Logger

[![CircleCI](https://circleci.com/bb/depax/depax-logger.svg?style=svg)](https://circleci.com/bb/depax/depax-logger)
[![Todos](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-logger/latest/artifacts/0/shields/todos.svg)](#)
[![Features](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-logger/latest/artifacts/0/shields/features.svg)](#)
[![Coverage](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-logger/latest/artifacts/0/shields/features-coverage.svg)](#)
[![Documentation](https://img.shields.io/badge/documentation-🕮-brightgreen.svg?style=plastic)](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-logger/latest/artifacts/0/documentation/index.html)
[![Report](https://img.shields.io/badge/report-💣-brightgreen.svg?style=plastic)](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-logger/latest/artifacts/0/report)

## Installation

Install the package normally using NPM or Yarn.

```sh
yarn add @depax/logger
```

## Logger Types

- *JsonLogger*: This logger type uses Bunyan and outputs the logs in a JSON format.
- *ConsoleLogger*: This logger outputs the logs to the console.
- *ChalkLogger*: This is the same as *ConsoleLogger* except it outputs the log type as a coloured string using chalk.
- *MemoryLogger*: This logger stores the logs in memory.
- *CombinedLogger*: This logger allows for multiple loggers within a single logger.
- *NullLogger*: This logger does not store any logs, its purpose is if you are only interested in the events.

## Usage

```js
import Logger, { JsonLogger } from "@depax/loggers";

async function setupLogger(): Promise<void> {
    const logger: Logger = new JsonLogger("test", { path: "/existing/path" });
    await logger.initialize();
}
```

Or for combined loggers;

```js
import Logger, { CombinedLogger, ConsoleLogger, MemoryLogger } from "@depax/loggers";

async function setupLogger(): Promise<void> {
    const logger: CombinedLogger = new CombinedLogger("test");
    logger.addLogger(new MemoryLogger("test1"));
    logger.addLogger(new ConsoleLogger("test2"));

    await logger.initialize();
}
```
