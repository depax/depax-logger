/**
 * Provides the main abstract Logger class.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Emitter = require("eventemitter3");

export enum LogType {
    Debug = "DEBUG",
    Error = "ERROR",
    Fatal = "FATAL",
    Info = "INFO",
    Warning = "WARN",
}

export default abstract class Logger extends Emitter {
    /** An internal flag to determine if debug logs are enabled. */
    protected debugging: boolean = false;

    /**
     * Construct the logger class.
     *
     * @param name The name of the logger.
     */
    public constructor(readonly name: string) {
        super();
    }

    /**
     * An initializer for the logger.
     */
    public async initialize(): Promise<void> { await this.doInitialize(); }

    /** Determine if debugging is enabled. */
    public get isDebugging(): boolean { return this.debugging; }
    public set isDebugging(value: boolean) { this.debugging = value === true; }

    /**
     * Log an "Information" log message.
     *
     * @param group A group or category for this log message.
     * @param message The log message.
     * @param args Any optional arguments to store along with the message, this is also used for translations.
     */
    public async info(message: string, args: any = {}): Promise<void> {
        await this.log(LogType.Info, message, args);
    }

    /**
     * Log a "Warning" log message.
     *
     * @param group A group or category for this log message.
     * @param message The log message.
     * @param args Any optional arguments to store along with the message, this is also used for translations.
     */
    public async warn(message: string, args: any = {}): Promise<void> {
        await this.log(LogType.Warning, message, args);
    }

    /**
     * Log an "Error" log message.
     *
     * @param group A group or category for this log message.
     * @param message The log message.
     * @param args Any optional arguments to store along with the message, this is also used for translations.
     */
    public async error(message: string, args: any = {}): Promise<void> {
        await this.log(LogType.Error, message, args);
    }

    /**
     * Log a "Success" log message.
     *
     * @param group A group or category for this log message.
     * @param message The log message.
     * @param args Any optional arguments to store along with the message, this is also used for translations.
     */
    public async fatal(message: string, args: any = {}): Promise<void> {
        await this.log(LogType.Fatal, message, args);
    }

    /**
     * Log a "Debug" log message.
     *
     * @param group A group or category for this log message.
     * @param message The log message.
     * @param args Any optional arguments to store along with the message, this is also used for translations.
     */
    public async debug(message: string, args: any = {}): Promise<void> {
        await this.log(LogType.Debug, message, args);
    }

    /**
     * Provides an internal log callback, this sanitizes the log message prior to sending it to the logger storage.
     *
     * @param type The type of log message.
     * @param message The log message.
     * @param args Any optional arguments to store along with the message, this is also used for translations.
     */
    public async log(type: LogType, message: string, args: any): Promise<void> {
        // Check if its a debug message, and debugging is enabled, if not then return false.
        if (type === LogType.Debug && !this.isDebugging) {
            return;
        }

        // Send the log message to the logger extender.
        await this.doLog(type, message, args);
        this.emit(type, { message, args });
    }

    /**
     * Allows the logger extender to perform any initialization.
     */
    protected abstract async doInitialize(): Promise<void>;

    /**
     * Provides an internal log callback, this performs the actions of storing the log message somewhere.
     *
     * @param type The type of log message.
     * @param message The log message.
     * @param args Any optional arguments to store along with the message, this is also used for translations.
     */
    protected abstract async doLog(type: LogType, message: string, args: any): Promise<void>;
}
