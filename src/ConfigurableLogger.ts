/**
 * Provides the abstract configurable Logger class.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Logger from "./Logger";

export interface ILoggerConfig {
    [name: string]: any;
}

export default abstract class ConfigurableLogger extends Logger {
    /** The configuration object for the Logger. */
    protected config: any;

    /**
     * Construct the logger class.
     *
     * @param name The name of the logger.
     * @param config The configuration details for the logger.
     */
    public constructor(readonly name: string, config: ILoggerConfig) {
        super(name);
        this.config = Object.assign(this.defaultConfig(), config);
    }

    /**
     * Allow the logger to provide a default config object.
     *
     * @return Returns the default config object.
     */
    protected abstract defaultConfig(): ILoggerConfig;
}
