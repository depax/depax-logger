/**
 * Provides a logger which outputs the logs to the console.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Logger, { LogType } from "../Logger";

export default class ConsoleLogger extends Logger {
    /** @inheritdoc */
    protected async doInitialize(): Promise<void> { /* Do nothing. */ }

    /** @inheritdoc */
    protected async doLog(type: LogType, message: string, args: any): Promise<void> {
        const logType = type === LogType.Fatal ? "error" : type.toLowerCase();
        const consoleArgs = [ message ];
        if (args && Object.keys(args).length) {
            consoleArgs.push(args);
        }

        console[logType].apply(console, consoleArgs);
    }
}
