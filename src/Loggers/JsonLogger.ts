/**
 * Provides a logger which will save to a rotated log file in JSON (using Bunyan).
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import bunyan = require("bunyan");
import ConfigurableLogger, { ILoggerConfig } from "../ConfigurableLogger";
import { LogType } from "../Logger";

export interface IJsonLoggerConfig extends ILoggerConfig {
    /** The directory to store these logs in. */
    path: string;

    /** The maximum number of rotated logs, the default is 3. */
    count?: number;

    /** The period in which the log is used before rotation, this defaults to 1 day "1d". */
    period?: string;
}

export default class JsonLogger extends ConfigurableLogger {
    /** An internal reference to the bunyan logger. */
    protected logger;

    /** @inheritdoc */
    public constructor(name: string, config: IJsonLoggerConfig) {
        super(name, config);
    }

    /** @inheritdoc */
    protected defaultConfig(): IJsonLoggerConfig {
        return {
            count: 3,
            path: "/var/log",
            period: "1d",
        };
    }

    /** @inheritdoc */
    protected async doInitialize(): Promise<void> {
        this.logger = bunyan.createLogger({
            level: 20,
            name: this.name,
            streams: [{
                count: this.config.maxFiles,
                path: `${this.config.path}/${this.name}.log`,
                period: this.config.period,
                type: "rotating-file",
            }],
        });
    }

    /** @inheritdoc */
    protected async doLog(type: LogType, message: string, args: any): Promise<void> {
        const logType = type.toLowerCase();
        this.logger[logType](args, message);
    }
}
