/**
 * Provides a logger which sends to logs to multiple defined loggers.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Logger, { LogType } from "../Logger";

export default class CombinedLogger extends Logger {
    /** A collection of loggers to send the events to. */
    protected loggers: Logger[] = [];

    /**
     * Add a logger to the collection.
     *
     * @param logger A reference to the logger to add to the collection.
     */
    public addLogger(logger: Logger): void { this.loggers.push(logger); }

    /** @inheritdoc */
    protected async doInitialize(): Promise<void> {
        for (const logger of this.loggers) {
            await logger.initialize();
        }
    }

    /** @inheritdoc */
    protected async doLog(type: LogType, message: string, args: any): Promise<void> {
        const logType = type.toLowerCase();
        this.loggers.forEach((logger) => logger[logType](message, args));
    }
}
