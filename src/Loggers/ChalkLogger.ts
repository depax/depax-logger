/**
 * Provides a logger which outputs the logs to the console with chalk colouring.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import chalk = require("chalk");
import { LogType } from "../Logger";
import ConsoleLogger from "./ConsoleLogger";

export default class ChalkLogger extends ConsoleLogger {
    /** @inheritdoc */
    protected async doLog(type: LogType, message: string, args: any): Promise<void> {
        const logType = type === LogType.Fatal ? "error" : type.toLowerCase();

        let colour = "green";
        switch (type) {
            case LogType.Error:
            case LogType.Fatal:
                colour = "red";
                break;

            case LogType.Warning:
                colour = "yellow";
                break;

            case LogType.Debug:
                colour = "cyan";
                break;
        }

        const status = (chalk as any).bold[colour](type.toUpperCase());
        const consoleArgs = [ `[ ${status} ] ${(chalk as any).white(message)}` ];
        if (args && Object.keys(args).length) {
            consoleArgs.push(args);
        }

        console[logType].apply(console, consoleArgs);
    }
}
