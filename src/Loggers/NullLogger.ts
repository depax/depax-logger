/**
 * Provides a logger which just does nothing with the logs, this allows us to just listen for events.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Logger, { LogType } from "../Logger";

export default class NullLogger extends Logger {
    /** @inheritdoc */
    protected async doInitialize(): Promise<void> { /* Do nothing. */ }

    /** @inheritdoc */
    protected async doLog(type: LogType, message: string, args: any): Promise<void> { /* Do nothing. */ }
}
