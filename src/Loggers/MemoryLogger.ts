/**
 * Provides a logger which just stores the logs in an internal array.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Logger, { LogType } from "../Logger";

export interface IMemoryLogRecord {
    args: any;
    date: number;
    message: string;
    type: LogType;
}

export default class MemoryLogger extends Logger {
    /** An internal reference to the stored logs. */
    private logs: IMemoryLogRecord[] = [];

    /** Get the internal logs. */
    public get getLogs(): IMemoryLogRecord[] { return this.logs; }

    /** @inheritdoc */
    protected async doInitialize(): Promise<void> { /* Do nothing. */ }

    /** @inheritdoc */
    protected async doLog(type: LogType, message: string, args: any): Promise<void> {
        const record: any = {};
        record.args = args;
        record.date = Date.now();
        record.message = message;
        record.type = type;

        this.logs.push(record);
    }
}
