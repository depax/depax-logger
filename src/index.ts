/**
 * Provides the route file of the module exposing all the relevant classes.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import ConfigurableLogger, { ILoggerConfig } from "./ConfigurableLogger";
import Logger, { LogType } from "./Logger";
import ChalkLogger from "./Loggers/ChalkLogger";
import CombinedLogger from "./Loggers/CombinedLogger";
import ConsoleLogger from "./Loggers/ConsoleLogger";
import JsonLogger from "./Loggers/JsonLogger";
import MemoryLogger from "./Loggers/MemoryLogger";
import NullLogger from "./Loggers/NullLogger";

export default Logger;
export {
    ConfigurableLogger, ILoggerConfig,
    LogType,
    ChalkLogger,
    CombinedLogger,
    ConsoleLogger,
    JsonLogger,
    MemoryLogger,
    NullLogger,
};
