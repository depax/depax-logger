/**
 * Provides the step definitions for the feature tests.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import {  } from "bunyan";
import chai = require("chai");
import { After, Before, Given, Then, When } from "cucumber";
import fs = require("fs");
import os = require("os");
import path = require("path");
import rimraf = require("rimraf");
import sinon = require("sinon");
import { promisify } from "util";
import Logger, {
    ChalkLogger,
    CombinedLogger,
    ConsoleLogger,
    JsonLogger,
    LogType,
    MemoryLogger,
    NullLogger,
} from "../../src";

const mkdir = promisify(fs.mkdir);
const readFile = promisify(fs.readFile);
const rmdir = promisify(rimraf);

const sleep = (ms) => new Promise((resolve) => setTimeout(() => resolve(ms), ms));

const tmpPath = path.join(os.tmpdir(), "json-logger");
const loggers: Map<string, Logger> = new Map();

let sandbox: sinon.SinonSandbox;

let nullLogs = [];

function addLog(type: LogType, ctx: { message: string, args: any }): void {
    nullLogs.push({
        args: ctx.args,
        date: "@ignore",
        message: ctx.message,
        type,
    });
}

//#region Define the Before and After steps
Before(async () => {
    loggers.clear();
    nullLogs = [];

    await mkdir(tmpPath);

    sandbox = sinon.createSandbox();
    sandbox.stub(console, "info");
    sandbox.stub(console, "warn");
    sandbox.stub(console, "error");
    sandbox.stub(console, "debug");
});

After(async () => {
    loggers.clear();

    sandbox.restore();

    await rmdir(tmpPath);
    await sleep(500);
});
//#endregion

//#region ----< Define the *Given* steps >------------------------------------------------------------------------------
Given(/I have a (Chalk|Console|Memory|Json|Combined|Null) logger called "(\S*)"$/, async (
    loggerType: string,
    id: string,
): Promise<void> => {
    let logger: Logger;
    switch (loggerType) {
        case "Chalk":
            logger = new ChalkLogger(id);
            break;

        case "Console":
            logger = new ConsoleLogger(id);
            break;

        case "Memory":
            logger = new MemoryLogger(id);
            break;

        case "Json":
            logger = new JsonLogger(id, { path: tmpPath });
            break;

        case "Combined":
            logger = new CombinedLogger(id);
            break;

        case "Null":
            logger = new NullLogger(id);
            break;
    }

    loggers.set(id, logger);

    if (logger instanceof JsonLogger) {
        chai.assert.propertyVal((logger as any).config, "path", tmpPath);
        chai.assert.propertyVal((logger as any).config, "period", "1d");
        chai.assert.propertyVal((logger as any).config, "count", 3);
    } else if (logger instanceof NullLogger) {
        logger
            .on(LogType.Info, (ctx) => addLog(LogType.Info, ctx))
            .on(LogType.Warning, (ctx) => addLog(LogType.Warning, ctx))
            .on(LogType.Error, (ctx) => addLog(LogType.Error, ctx))
            .on(LogType.Fatal, (ctx) => addLog(LogType.Fatal, ctx))
            .on(LogType.Debug, (ctx) => addLog(LogType.Debug, ctx));
    }
});

Given(/^I initialize the logger "(\S*)"$/, async (
    id: string,
): Promise<void> => {
    if (loggers.has(id) === false) {
        throw new Error(`Unknown logger "${id}".`);
    }

    await loggers.get(id).initialize();
});

Given(/^I enable debugging on "(\S*)"$/, async (
    id: string,
): Promise<void> => {
    if (loggers.has(id) === false) {
        throw new Error(`Unknown logger "${id}".`);
    }

    loggers.get(id).isDebugging = true;
});

Given(/^I add "(\S*)" to "(\S*)"$/, async (
    childId: string,
    parentId: string,
): Promise<void> => {
    if (loggers.has(childId) === false) {
        throw new Error(`Unknown logger "${childId}".`);
    }

    if (loggers.has(parentId) === false) {
        throw new Error(`Unknown logger "${parentId}".`);
    }

    const logger = loggers.get(parentId);
    if (logger instanceof CombinedLogger === false) {
        throw new Error("Parent ID is expected to be a combined logger type.");
    }

    (logger as CombinedLogger).addLogger(loggers.get(childId));
});
//#endregion

//#region ----< Define the *When* steps >-------------------------------------------------------------------------------
When(/^I send an (info|warn|error|fatal|debug) log to "(\S*)" with "(.*)"$/, async (
    logType: string,
    id: string,
    msg: string,
): Promise<void> => {
    if (loggers.has(id) === false) {
        throw new Error(`Unknown logger "${id}".`);
    }

    await loggers.get(id)[logType](msg);
});

When(/^I send an (info|warn|error|fatal|debug) log to "(\S*)" with "(.*)" and args;$/, async (
    logType: string,
    id: string,
    msg: string,
    args: string,
): Promise<void> => {
    if (loggers.has(id) === false) {
        throw new Error(`Unknown logger "${id}".`);
    }

    await loggers.get(id)[logType](msg, JSON.parse(args));
});
//#endregion

//#region ----< Define the *Then* steps >-------------------------------------------------------------------------------
Then(/^The logger "(\S*)" has debug (enabled|disabled)$/, async (
    id: string,
    enabled: string,
) => {
    if (loggers.has(id) === false) {
        throw new Error(`Unknown logger "${id}".`);
    }

    chai.assert[enabled === "enabled" ? "isTrue" : "isFalse"](loggers.get(id).isDebugging);
});

Then(/^The logs output for "(\S*)" are as follows;$/, async (
    id: string,
    output: string,
): Promise<void> => {
    if (loggers.has(id) === false) {
        throw new Error(`Unknown logger "${id}".`);
    }

    const logger: Logger = loggers.get(id);
    if (logger instanceof MemoryLogger) {
        const loggerLogs = (logger as MemoryLogger).getLogs;
        loggerLogs.forEach((log: any) => log.date = "@ignore");

        chai.assert.deepEqual(loggerLogs, JSON.parse(output));
    } else if (logger instanceof JsonLogger) {
        const fileContents = (await readFile(path.join(tmpPath, `${id}.log`))).toString();
        const fileLogs = !fileContents ? false : JSON.parse(fileContents);
        const loggerLogs = [];

        if (fileLogs) {
            const args = Object.assign({}, fileLogs);
            delete args.name;
            delete args.hostname;
            delete args.pid;
            delete args.level;
            delete args.msg;
            delete args.time;
            delete args.v;

            let level;
            switch (fileLogs.level) {
                case 60:
                    level = "FATAL";
                    break;

                case 50:
                    level = "ERROR";
                    break;

                case 40:
                    level = "WARN";
                    break;

                case 20:
                    level = "DEBUG";
                    break;

                default:
                    level = "INFO";
                    break;
            }

            loggerLogs.push({
                args,
                date: "@ignore",
                message: fileLogs.msg,
                type: level,
            });
        }

        chai.assert.deepEqual(loggerLogs, JSON.parse(output));
    } else if (logger instanceof NullLogger) {
        chai.assert.deepEqual(nullLogs, JSON.parse(output));
    } else {
        throw new Error("This logger type is not handled here.");
    }
});

Then(/^The console function "(\S*)" was not called$/, async (
    fnc: string,
): Promise<void> => {
    sinon.assert.notCalled(console[fnc]);
});

Then(/^The console function "(\S*)" was called with "(.*)"$/, async (
    fnc: string,
    msg: string,
): Promise<void> => {
    sinon.assert.calledOnce(console[fnc] as any);
    sinon.assert.calledWith(console[fnc] as any, msg);
});

Then(/^The console function "(\S*)" was called with "(.*)" and args;$/, async (
    fnc: string,
    msg: string,
    output: string,
): Promise<void> => {
    sinon.assert.calledOnce(console[fnc] as any);
    sinon.assert.calledWith(console[fnc] as any, msg, JSON.parse(output));
});
//#endregion
