@null-logger
Feature: Null Logger

  Scenario: Should log "info" messages
    Given I have a Null logger called "test"
      And I initialize the logger "test"
     When I send an info log to "test" with "Hello world"
     Then The logs output for "test" are as follows;
      """
      [{
          "args": {},
          "date": "@ignore",
          "message": "Hello world",
          "type": "INFO"
      }]
      """

  Scenario: Should log "info" messages with arguments
    Given I have a Null logger called "test"
      And I initialize the logger "test"
     When I send an info log to "test" with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
     Then The logs output for "test" are as follows;
      """
      [{
          "args": {
              "arg1": "test",
              "arg2": "foo"
          },
          "date": "@ignore",
          "message": "Hello world",
          "type": "INFO"
      }]
      """

  Scenario: Should log "warn" messages
    Given I have a Null logger called "test"
      And I initialize the logger "test"
     When I send an warn log to "test" with "Hello world"
     Then The logs output for "test" are as follows;
      """
      [{
          "args": {},
          "date": "@ignore",
          "message": "Hello world",
          "type": "WARN"
      }]
      """

  Scenario: Should log "warn" messages with arguments
    Given I have a Null logger called "test"
      And I initialize the logger "test"
     When I send an warn log to "test" with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
     Then The logs output for "test" are as follows;
      """
      [{
          "args": {
              "arg1": "test",
              "arg2": "foo"
          },
          "date": "@ignore",
          "message": "Hello world",
          "type": "WARN"
      }]
      """

  Scenario: Should log "error" messages
    Given I have a Null logger called "test"
      And I initialize the logger "test"
     When I send an error log to "test" with "Hello world"
     Then The logs output for "test" are as follows;
      """
      [{
          "args": {},
          "date": "@ignore",
          "message": "Hello world",
          "type": "ERROR"
      }]
      """

  Scenario: Should log "error" messages with arguments
    Given I have a Null logger called "test"
      And I initialize the logger "test"
     When I send an error log to "test" with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
     Then The logs output for "test" are as follows;
      """
      [{
          "args": {
              "arg1": "test",
              "arg2": "foo"
          },
          "date": "@ignore",
          "message": "Hello world",
          "type": "ERROR"
      }]
      """

  Scenario: Should log "fatal" messages
    Given I have a Null logger called "test"
      And I initialize the logger "test"
     When I send an fatal log to "test" with "Hello world"
     Then The logs output for "test" are as follows;
      """
      [{
          "args": {},
          "date": "@ignore",
          "message": "Hello world",
          "type": "FATAL"
      }]
      """

  Scenario: Should log "fatal" messages with arguments
    Given I have a Null logger called "test"
      And I initialize the logger "test"
     When I send an fatal log to "test" with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
     Then The logs output for "test" are as follows;
      """
      [{
          "args": {
              "arg1": "test",
              "arg2": "foo"
          },
          "date": "@ignore",
          "message": "Hello world",
          "type": "FATAL"
      }]
      """

  Scenario: Should not log "debug" messages if not enabled
    Given I have a Null logger called "test"
      And I initialize the logger "test"
     When I send an debug log to "test" with "Hello world"
     Then The logger "test" has debug disabled
      And The logs output for "test" are as follows;
      """
      []
      """

  Scenario: Should not log "debug" messages with arguments if not enabled
    Given I have a Null logger called "test"
      And I initialize the logger "test"
     When I send an debug log to "test" with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
     Then The logs output for "test" are as follows;
      """
      []
      """

  Scenario: Should log "debug" messages if enabled
    Given I have a Null logger called "test"
      And I initialize the logger "test"
      And I enable debugging on "test"
     When I send an debug log to "test" with "Hello world"
     Then The logger "test" has debug enabled
      And The logs output for "test" are as follows;
      """
      [{
          "args": {},
          "date": "@ignore",
          "message": "Hello world",
          "type": "DEBUG"
      }]
      """

  Scenario: Should log "debug" messages with arguments if enabled
    Given I have a Null logger called "test"
      And I initialize the logger "test"
      And I enable debugging on "test"
     When I send an debug log to "test" with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
     Then The logger "test" has debug enabled
      And The logs output for "test" are as follows;
      """
      [{
          "args": {
              "arg1": "test",
              "arg2": "foo"
          },
          "date": "@ignore",
          "message": "Hello world",
          "type": "DEBUG"
      }]
      """
