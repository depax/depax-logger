@console-logger
Feature: Console Logger

  Scenario: Should log "info" messages
    Given I have a Console logger called "test"
      And I initialize the logger "test"
     When I send an info log to "test" with "Hello world"
     Then The console function "info" was called with "Hello world"

  Scenario: Should log "info" messages with arguments
    Given I have a Console logger called "test"
      And I initialize the logger "test"
     When I send an info log to "test" with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
     Then The console function "info" was called with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """

  Scenario: Should log "warn" messages
    Given I have a Console logger called "test"
      And I initialize the logger "test"
     When I send an warn log to "test" with "Hello world"
     Then The console function "warn" was called with "Hello world"

  Scenario: Should log "warn" messages with arguments
    Given I have a Console logger called "test"
      And I initialize the logger "test"
     When I send an warn log to "test" with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
     Then The console function "warn" was called with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """

  Scenario: Should log "error" messages
    Given I have a Console logger called "test"
      And I initialize the logger "test"
     When I send an error log to "test" with "Hello world"
     Then The console function "error" was called with "Hello world"

  Scenario: Should log "error" messages with arguments
    Given I have a Console logger called "test"
      And I initialize the logger "test"
     When I send an error log to "test" with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
     Then The console function "error" was called with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """

  Scenario: Should log "fatal" messages
    Given I have a Console logger called "test"
      And I initialize the logger "test"
     When I send an fatal log to "test" with "Hello world"
     Then The console function "error" was called with "Hello world"

  Scenario: Should log "fatal" messages with arguments
    Given I have a Console logger called "test"
      And I initialize the logger "test"
     When I send an fatal log to "test" with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
     Then The console function "error" was called with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """

  Scenario: Should not log "debug" messages if not enabled
    Given I have a Console logger called "test"
      And I initialize the logger "test"
     When I send an debug log to "test" with "Hello world"
     Then The logger "test" has debug disabled
      And The console function "debug" was not called

  Scenario: Should not log "debug" messages with arguments if not enabled
    Given I have a Console logger called "test"
      And I initialize the logger "test"
     When I send an debug log to "test" with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
     Then The console function "debug" was not called

  Scenario: Should log "debug" messages if enabled
    Given I have a Console logger called "test"
      And I initialize the logger "test"
      And I enable debugging on "test"
     When I send an debug log to "test" with "Hello world"
     Then The logger "test" has debug enabled
      And The console function "debug" was called with "Hello world"

  Scenario: Should log "debug" messages with arguments if enabled
    Given I have a Console logger called "test"
      And I initialize the logger "test"
      And I enable debugging on "test"
     When I send an debug log to "test" with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
     Then The logger "test" has debug enabled
      And The console function "debug" was called with "Hello world" and args;
      """
      {
          "arg1": "test",
          "arg2": "foo"
      }
      """
